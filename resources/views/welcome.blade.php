@extends('layouts.app')

@section('content')
<div class="container spark-screen">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Bem Vindo</div>

                <div class="panel-body">
                    Página de destino da sua aplicação.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
