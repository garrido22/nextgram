@extends('layouts.app')
@section('title','Todos as Postagens')

@section('content')
    <div class="wrapper">
        @include('components.navigation')
        @include('components.sidebar')
        <div id="allpost"></div>
        <div class="content-wrapper">
            <section class="content">
                <div class="box">
                    <div class="box-header">
                        <h3 class="box-title"> Todos os artigos e disponibilidade <label
                                    class="badge">{{\App\Allpost::all()->count()}}</label></h3>
                        <button id="delall" class="btn btn-warning btn-xs"><i class="fa fa-database"></i> Apague Todos os Dados
                        </button>
                    </div>
                    <div class="box-body">
                        <table id="mytable" class="table table-bordered table-striped" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>ID Postagem</th>
                                <th>Titulo</th>
                                <th>Conteudo</th>
                                <th>
                                    <div align="center">Publicado em</div>
                                </th>
                                <th>
                                    <div align="center">Ação</div>
                                </th>
                            </tr>
                            </thead>

                            <tbody>
                            @foreach($posts as $post)
                                <tr>
                                    <td>{{$post->postId}}</td>
                                    <td>{{$post->title}}</td>
                                    <td>{{\App\Http\Controllers\Data::shortText($post->content)}}</td>

                                    <td align="center">
                                        @if(\App\Fb::where('postId',$post->postId)->exists())
                                            <i class="fa fa-facebook"></i> &nbsp;
                                        @endif
                                        @if(\App\Tw::where('postId',$post->postId)->exists())
                                            <i class="fa fa-twitter"></i> &nbsp;
                                        @endif
                                        @if(\App\Wp::where('postId',$post->postId)->exists())
                                            <i class="fa fa-wordpress"></i> &nbsp;
                                        @endif
                                        @if(\App\Tu::where('postId',$post->postId)->exists())
                                            <i class="fa fa-tumblr"></i>
                                        @endif
                                        @if(\App\Fbgr::where('postId',$post->postId)->exists())
                                            <i class="fa fa-users"></i>
                                        @endif

                                    </td>

                                    <td align="center">
                                        <button data-id="{{$post->id}}" class="btn btn-danger btn-xs"><i
                                                    class="fa fa-trash"></i> Deletar
                                        </button>
                                        <button data-id="{{$post->id}}" data-value="{{$post->postId}}"
                                                class="btn btn-primary optdelall btn-xs"><i class="fa fa-times"> Deletar Todos </i></button>

                                    </td>
                                </tr>
                            @endforeach
                            </tbody>

                            <tfoot>
                            <tr>
                                <th>ID Postagem</th>
                                <th>Titulo</th>
                                <th>Conteudo</th>
                                <th>Disponivel em</th>
                                <th>Ação</th>
                            </tr>
                            </tfoot>
                        </table>
                    </div>
                </div>{{--End box--}}
            </section>{{--End content--}}
        </div>{{--End content-wrapper--}}
        @include('components.footer')
    </div>{{--End wrapper--}}
@endsection
@section('css')
    <script src="{{url('/opt/sweetalert.min.js')}}"></script>
    <link rel="stylesheet" type="text/css" href="{{url('/opt/sweetalert.css')}}">
@endsection

@section('js')
    <script>
        $('.optdelall').click(function () {

            var postId = $(this).attr('data-value');

            $(this).html("Por favor aguarde..");
            $.ajax({
                type: 'POST',
                url: '{{url('/delfromall')}}',
                data: {
                    'postId': postId
                },
                success: function (data) {
                    swal("Status", data);
                    notify('{{url('/images/optimus/social/logopadding.png')}}', 'Message', data, '#');
                    $('.optdelall').html("<i class='fa fa-times'> Deletar Todos </i>");
                    location.reload();

                }
            });


        })
    </script>
@endsection