<?php
return [
    "Select your language"=>"Selecione sua linguagem",
    "See All Messages"=>"Ver todas as mensagens",
    "See All Notifications"=>"Ver todas as notificações",
    "Profile"=>"Perfil",
    "Sign out"=>"Deslogar"
];