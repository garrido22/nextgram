<?php
return [
    "Dashboard"=>"Painel de Controle",
    "page likes"=>"Paginas Curtidas",
    "Loading" => "Carregando",
    "followers"=>"Seguidores",
    "company followers"=>"Seguidores da Empresa",
    "Total Logs"=>"Total Logs",
    "Blogs"=>"Blogs",
    "Facebook"=>"Facebook",
    "Pages"=>"Paginas",
    "Tumblr"=>"Tumblr",
    "Groups"=>"Groupos",
    "Posted Jobs"=>"Serviços Postados",
    "Company Updates"=>"Atualizações da Empresa",
    "Fllowing"=>"Segue",
    "Instagram"=>"Instagram",
    "Facebook page Post"=>"Posts do Facebook",
    "Tumblr Posts"=>"Tumblr Posts",
    "Twitter Posts"=>"Twitter Posts",
    "Facebook Group Post"=>"Posts do Grupo",
    "All Posts"=>"Todos os Post"
];