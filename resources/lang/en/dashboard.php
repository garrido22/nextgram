<?php
return [
    "Dashboard"=>"Painel de Controle",
    "page likes"=>"Total de Curtidas",
    "Loading" => "Carregando",
    "followers"=>"Seguidores",
    "company followers"=>"Seguidores da Empresa",
    "Total Logs"=>"Total Logs",
    "Blogs"=>"Blogs",
    "Facebook"=>"Facebook",
    "Pages"=>"Paginas",
    "Tumblr"=>"Tumblr",
    "Groups"=>"Groupos",
    "Posted Jobs"=>"Serviços Postados",
    "Company Updates"=>"Atualizações da Empresa",
    "Fllowing"=>"Seguidores",
    "Instagram"=>"Instagram",
    "Facebook page Post"=>"Posts do Facebook",
    "Tumblr Posts"=>"Tumblr Posts",
    "Twitter Posts"=>"Twitter Posts",
    "Facebook Group Post"=>"Posts dos Grupos",
    "All Posts"=>"Todos os Post"
];